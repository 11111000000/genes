#+TITLE: Sitemap for project site-org

- [[file:bio.org][Генетическое выравнивание]]
- [[file:emacs/emacs.org][emacs]]
- [[file:books/orgpapers.org][Using Emacs, Org-mode and R for Research Writing in Social Sciences]]
- [[file:books/default_packages.org][default_packages]]