# -*- eval: (let () (org-babel-goto-named-src-block "emacs") (org-babel-execute-src-block)); -*-

# Test

#+TITLE: Генетическое выравнивание
#+AUTHOR: Peter Kosov, Malvina Shlepneva, Max Miakishev-Rempel
#+SUBTITLE: Поиск подобия в структурах хроматина

#+OPTIONS: html-style:nil html5-fancy:t

#+OPTIONS: num:nil

#+HTML_DOCTYPE: html5
#+HTML_HEAD: <meta http-equiv="X-UA-Compatible" content="IE=edge">
#+HTML_HEAD: <meta name="viewport" content="width=device-width, initial-scale=1">

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://www.pirilampo.org/styles/bigblow/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://www.pirilampo.org/styles/bigblow/css/bigblow.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://www.pirilampo.org/styles/bigblow/css/hideshow.css"/>

#+HTML_HEAD: <script type="text/javascript" src="https://www.pirilampo.org/styles/bigblow/js/jquery-1.11.0.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="https://www.pirilampo.org/styles/bigblow/js/jquery-ui-1.10.2.min.js"></script>

#+HTML_HEAD: <script type="text/javascript" src="https://www.pirilampo.org/styles/bigblow/js/jquery.localscroll-min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="https://www.pirilampo.org/styles/bigblow/js/jquery.scrollTo-1.4.3.1-min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="https://www.pirilampo.org/styles/bigblow/js/jquery.zclip.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="https://www.pirilampo.org/styles/bigblow/js/bigblow.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="https://www.pirilampo.org/styles/bigblow/js/hideshow.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="https://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.min.js"></script>

* Задачи [50%]

Чтобы отметить задачу с отметкой выполнения: 
 - подвести курсор на задачу
 - нажать *C-x C-t x*

** DONE Dropbox                                               :peter:malvina:
** Настройка среды [50%]                                      :peter:malvina:
*** DONE Настройка EMACS и org-mode                                   :peter:
*** TODO Установить Bio Linux                                 :peter:malvina:
** DONE Сбор материалов                                               :peter:
** DONE Нарисовать графики на R                                       :peter:
** DONE Нарисовать гены на R                                          :peter:
** DONE Переговоры в 10:00 Пт.                                        :peter:
** TODO Ресёрч и анализ [16%]
*** DONE Прослушать вводную лекцию                                    :peter:
    - State "DONE"       from "TODO"       [2017-12-01 Fri 12:37]
*** TODO Научиться пользоваться org TODO списками                   :malvina:
*** TODO Прослушать вводную лекцию                                  :malvina:
*** TODO Прочитать материалы                                        :malvina:
*** TODO Подготовить вопросы                                        :malvina:
*** TODO Собрать и описать входные данные                           :malvina:
*** TODO Найти альтернативные источники данных
*** TODO Найти похожие исследования высокоурвневых структур DNA     :malvina:
*** TODO Найти походие исследования высокоурвневых структур DNA       :peter:
*** 
** TODO Выровнять цепочку генов
** TODO Постановка задач
** TODO Подготовка данных
** TODO ...
** TODO Профит

* EMACS
** Настройки

 При отрытии этого файла в EMACS, он предложит выполнить эти настройки.
 А чтобы ещё раз применить настройки нужно подвести курсор на этот блок кода и нажать (*C-c C-c*)
 Либо открыть emacs/emacs.el и набрать *<M-x> eval-buffer*

#+NAME: emacs
#+BEGIN_SRC emacs-lisp
(org-babel-load-file
 (expand-file-name "./emacs/emacs.org"))
#+END_SRC

#+RESULTS: emacs
: Loaded /home/az/Dropbox/genes/emacs/emacs.el

#+RESULTS:
: Loaded /home/az/Dropbox/genes/emacs/emacs.el

** Используемые волшебные клавиши

 | клавиши   | функция                                           |
 |-----------+---------------------------------------------------|
 | <C-c C-c> | самая волшебная (оживает что угодно под курсором) |
 | C-g       | отмена                                            |
 | M-x       | комманды                                          |
 | <C-x C-s> | сохранить                                         |
 | <C-x 1>   | текущее на весь экран                             |
 | <C-x 2>   | разделить на 2 окна                               |
 | <C-x o>   | прыгать между окнами                              |
 | <C-r>     | обновление буфера                                 |

* Теория
** Генетическое выравнивание
*** Определение

биоинформатический метод, основанный на размещении двух или более последовательностей мономеров ДНК, РНК или белков друг под другом таким образом, чтобы легко увидеть сходные участки в этих последовательностях. Сходство Первичная структура двух молекул может отражать их функциональные, структурные или эволюционные взаимосвязи. Выровненные последовательности оснований нуклеотидов или аминокислот обычно представляются в виде строк матрицы. Добавляются разрывы между основаниями таким образом, чтобы одинаковые или похожие элементы были расположены в следующих друг за другом столбцах матрицы.

Алгоритмы выравнивания последовательностей также используются в Обработка естественного языка.

*** Графическое и текстовое представление

В большинстве представлений результата выравнивания, последовательности располагаются в строчках матрицы таким образом, что совпадающие элементы (нуклеотиды или аминокислоты) находятся один под другим (в одной колонке). «Разрывы» заменяются знаком «-» или ячейка остается пустой.

[[file:images/seq-alignment.png]]

^^^ Выравнивание двух последовательностей аминокислот («цинковых пальцев»), сгенерированное программой ClustalW. В левой колонке находятся идентификаторы этих последовательностей в базе GenBank.
Обозначения: Буквы — Аминокислоты. Знаки под колонками: «*» — одинаковые; «.» — сходные по свойствам; «:» — близкие по свойствам

*** *Парное выравнивание*

Парное выравнивание используется для нахождения сходных участков двух последовательностей. Различают глобальное и локальное выравнивание. Глобальное выравнивание предполагает, что последовательности гомологичны по всей длине. В глобальное выравнивание включаются обе входные последовательности целиком. Локальное выравнивание применяется, если последовательности содержат как родственные (гомологичные), так и неродственные участки. Результатом локального выравнивания является выбор участка в каждой из последовательностей и выравнивание между этими участками.
Для получения парного выравнивания используются разновидности метода динамического программирования: для глобального выравнивания — алгоритм Нидлмана — Вунша, для локального — алгоритм Смита — Ватермана.

*** *Алгоритмы поиска*

Применяются для поиска в больших базах данных последовательностей, схожих с некой заданной последовательностью по указанным критериям. Применяемое выравнивание — локальное. Для повышения скорости поиска используются различные эвристические методы. Наиболее известные программы: BLAST и FASTA3x.

*** [[https://ru.wikipedia.org/wiki/%25D0%259C%25D0%25BD%25D0%25BE%25D0%25B6%25D0%25B5%25D1%2581%25D1%2582%25D0%25B2%25D0%25B5%25D0%25BD%25D0%25BD%25D0%25BE%25D0%25B5_%25D0%25B2%25D1%258B%25D1%2580%25D0%25B0%25D0%25B2%25D0%25BD%25D0%25B8%25D0%25B2%25D0%25B0%25D0%25BD%25D0%25B8%25D0%25B5_%25D0%25BF%25D0%25BE%25D1%2581%25D0%25BB%25D0%25B5%25D0%25B4%25D0%25BE%25D0%25B2%25D0%25B0%25D1%2582%25D0%25B5%25D0%25BB%25D1%258C%25D0%25BD%25D0%25BE%25D1%2581%25D1%2582%25D0%25B5%25D0%25B9][Множественное выравнивание]]

Множественное выравнивание — это выравнивание трёх и более последовательностей. Применяется для нахождения консервативных участков в наборе гомологичных последовательностей. В большинстве случаев построение множественного выравнивания — необходимый этап реконструкции филогенетических деревьев. Нахождение оптимального множественного выравнивания методом динамического программирования имеет слишком большую временную сложность, поэтому множественные выравнивания строятся на базе различных эвристик. Наиболее известные программы, осуществляющие множественное выравнивание — Clustal (http://www.clustal.org/), T-COFFEE (англ.) (http://www.tcoffee.org), MUSCLE (англ.) (http://www.drive5.com/muscle/) и MAFFT (англ.) (http://mafft.cbrc.jp/alignment/software/). Имеются также программы для просмотра и редактирования множественных выравниваний, например Jalview (англ.) или русскоязычный UGENE.

*** Структурное выравнивание

Применяется к белкам и рибонуклеиновым кислотам (РНК), для которых известна пространственная (третичная) структура. Целью является нахождение и сопоставление участков, одинаково уложенных в пространстве. Структурное выравнивание обычно сопровождается наложением структур, то есть нахождением движений пространства, применение которых к заданным молекулам наилучшим образом совмещает их. Имеется большое количество программ для структурного выравнивания (англ.).

*** [[http://wiki.bioinformaticsinstitute.ru/wiki/%25D0%2592%25D1%258B%25D1%2580%25D0%25B0%25D0%25B2%25D0%25BD%25D0%25B8%25D0%25B2%25D0%25B0%25D0%25BD%25D0%25B8%25D0%25B5_%25D1%2580%25D0%25B8%25D0%25B4%25D0%25BE%25D0%25B2_%25D0%25BD%25D0%25B0_%25D1%2580%25D0%25B5%25D1%2584%25D0%25B5%25D1%2580%25D0%25B5%25D0%25BD%25D1%2581%25D0%25BD%25D1%258B%25D0%25B9_%25D0%25B3%25D0%25B5%25D0%25BD%25D0%25BE%25D0%25BC][Выравнивание ридов на референсный геном]]

   [[file:images/calling.png]]

* Исходные данные
 
 Сюда собираем информацию о входных данных

** Задачи [0%]
*** TODO Найти и изучить "красный файл"                       :malvina:peter:
*** TODO Найти другие источники аналогичных данных                  :malvina:
*** TODO Сложить все данные в папку [[file:./data]]

* Софт
** [[https://geektimes.ru/post/170429/][Браузеры Генома]]
** ClustalW
ClustalW - Программа для множественного выравнивания последовательностей ДНК и белков.  Работает в два этапа. Первый - попарное выравнивание всех со всеми для оценки сходства. Второй этап - после построения филогенетического дерева выравнивание глобальное. Отличие программы ClustalW от ClustalX состоит в том, что в ClustalX используется графический интерфейс, в то время как ClustalW  работает через командную строку или он-лайн.

** MUSCLE
Одна из самых быстрых и эффективных программ для множественного выравнивания белковых и нуклеотидных последовательностей. Согласно тестам MUSCLE достигает очень высоких результатов на системах тестирования качества множественного выравнивания, в основе которых лежат общепризанные базы известных выравниваний prefab и balibase.

MUSCLE это консольная утилита работающая преимущественно с FASTA и CLUSTAL форматами данных.
** MUMMER
MUMmer это система для быстрого выравнивания больших геномных последовательностей.

Например, MUMmer 3.0 может найти все повторы длиной 20 или больше в геноме размером 5Мб за несколько секунд используя при этом порядка 80Мб оперативной памятию. MUMmer может также использоваться для сборки геномов из фрагментов.
** HMMER
HMMER это реализация метода поиска в базах биологических последовательностей с использованием в качестве запроса к базе профиля множественном выравнивания. В основе алгоритмов построения и поиска пакета HMMER лежит теория скрытых марковских моделей, имитирующих работу процесса, похожего на марковский процесс с неизвестными параметрами, и задачей ставится разгадывание неизвестных параметров на основе наблюдаемых.

Пакет HMMER содержит набор консольных программ для построения статистических HMM профилей и поиска в биологических последовательностях при помощи уже готовых HMM профилей.
** DIALIGN-TX
DIALIGN-TX это консольная утилита для множественного выравнивания последовательностей ДНК и белков.

Программа представляет из себя полную реализацию алгоритма выравнивания основанного на независимой обработке отдельных сегментов и имеет ряд улучшений и эвристик которые значительно улучшают качество результирующего выравнивания в сравнении с предыдущими версиями программы DIALIGN 2.2 и DIALIGN-T.
** [[http://software.broadinstitute.org/software/igv/][IGV (Integrated Genomics Viewer)]]
  Просмотр Генома (установлено в nix)
** [[http://stothard.afns.ualberta.ca/cgview_server/][CGView Server]] 
генерация круговых диаграмм генома
** PyMOL
PyMol (PyMOL Molecular Graphics System) - это графическая програма, которая предоставляет 3D визуализацию для белков, небольших молекул, молекулярных поверхностей и траекторий.
** RasMol
RasMol — компьютерная программа, предназначенная для визуализации молекул и используемая преимущественно для изучения и получения изображений пространственных структур биологических макромолекул, в первую очередь белков и нуклеиновых кислот.
** Raster3D
Raster3D - это набор инструментов для генерации высококачественных растровых изображений протеинов и других молекул.
** UGENE 
UGENE - пакет для работы молекулярного биолога, предоставляющий инструменты для множественного выравнивания нуклеотидных и аминокислотных последовательностей, филогенетического анализа, редактирования и аннотирования нуклеотидных и белковых последовательностей, поддержания биоинформационной базы данных, визуализации, поиска геномных вариаций, работы с хроматограммами и многого другого.
** VMD
VMD (Visual Molecular Dynamics - Визуальная Молекулярная Динамика) разработан специально для визуализации и анализа таких биологических систем, как белки, нуклеиновые кислоты, молекулярные системы на основе липидов (например, компоненты клеточных мембран).
** FastQC 
контроль качества

** UGENE
*UGENE* это свободное ПО для работы молекулярного биолога. Пакет является кроссплатформенным, локализован на русский язык.

*Основные вычислительные возможности*:

Множественное выравнивание последовательностей на основе MUSCLE 3 и MUSCLE 4;
Анализ с помощью скрытых марковских моделей, основанный на HMMER 2 и HMMER 3;
Дизайн ПЦР-праймеров с помощью Primer 3;
Предсказание вторичной структуры белков с помощью GOR IV и PSIPRED;
Поиск сайтов рестрикции;
Сверхбыстрый поиск повторов;
Анализ сайтов связывания транскрипционных факторов на основе SITECON;
Поиск открытых рамок считывания;
Выравнивание последовательностей с помощью алгоритма Смита-Ватермана.

*Пользовательский интерфейс*:

Визуализация хроматограмм;
Просмотр трехмерных моделей PDB и MMDB;
Редактор множественного выравнивания;
Конструктор вычислительных схем автоматизирующий процесс анализа;
Поддержка сохранения изображений в векторные форматы для удобства публикации.

* Практика
** Проверка работоспособности R

#+BEGIN_SRC R :file images/R1.png :results output graphics
library(lattice)
xyplot(1:1 ~ 10:10)
#+END_SRC

#+RESULTS:
[[file:images/R1.png]]

*** Визуализация генома посредством genoPlotR
**** Библиотека

#+BEGIN_SRC R :tangle no
install.packages("ade4", lib='./R', repos="http://R-Forge.R-project.org")
install.packages("genoPlotR", lib='./R', repos="http://R-Forge.R-project.org")
library('ade4',lib='./R')
library('genoPlotR',lib='./R')
#+END_SRC

**** Программа на R

#+BEGIN_SRC R

##
## субсегмент Бартонеллы
##

## Загрузка библиотеки и данных

library('ade4',lib='./R')
library('genoPlotR',lib='./R')

data(barto)

## Пути для сохранения
imgPath <- "./images"
pdfPath <- "./pdfs"

## Сегмент 2
xlim_ref <- c(10000, 45000)
barto$dna_segs[[2]] <- trim(barto$dna_segs[[2]], xlim=xlim_ref)

## Сегмент 1

barto$comparisons[[1]] <- trim(barto$comparisons[[1]], xlim2=xlim_ref)
xlim1 <- range(barto$comparisons[[1]], overall=FALSE)$xlim1
barto$dna_segs[[1]] <- trim(barto$dna_segs[[1]], xlim=xlim1)

## Сегмент 3

barto$comparisons[[2]] <- trim(barto$comparisons[[2]], xlim1=xlim_ref)
xlim3 <- range(barto$comparisons[[2]], overall=FALSE)$xlim2
barto$dna_segs[[3]] <- trim(barto$dna_segs[[3]], xlim=xlim3)

## Сегмент 4

barto$comparisons[[3]] <- trim(barto$comparisons[[3]], xlim1=xlim3)
xlim4 <- range(barto$comparisons[[3]], overall=FALSE)$xlim2
barto$dna_segs[[4]] <- trim(barto$dna_segs[[4]], xlim=xlim4)

## Аннотации

mids <- apply(barto$dna_segs[[1]][c("start", "end")], 1, mean)
text <- barto$dna_segs[[1]]$name
text[grep("BARBAKC", text)] <- ""
annot <- annotation(x1=mids, text=text, rot=30)

## Дерево

tree <- newick2phylog("(BB:2.5,(BG:1.8,(BH:1,BQ:0.8):1.9):3);")

## Рисуем

png(file.path(imgPath, "barto_seg1.png"), h=300, w=500)
plot_gene_map(barto$dna_segs, 
              barto$comparisons,
              tree=tree,
              annotations=annot,
              dna_seg_scale=c(rep(FALSE, 3), TRUE),
              scale=FALSE,
              main="Сравнение того-же сегмента в 4 геномах Bartonella")
dev.off()

## Также, сгенерим PDF-ку

cairo_pdf(file.path(pdfPath, "barto_seg1.pdf"), h=4, w=7)
plot_gene_map(barto$dna_segs, barto$comparisons, tree=tree,
              annotations=annot, dna_seg_scale=c(rep(FALSE, 3), TRUE),
              scale=FALSE,
              main="Сравнение того-же сегмента в 4 геномах Bartonella")
dev.off()

 #+END_SRC

 #+RESULTS:
 : 1

**** Результат

#+CAPTION: Barto Seg 1
#+ATTR_HTML: :alt barto_seg_1 image :title Barto! :align center
[[file:./images/barto_seg1.png]]

**** Ещё сегменты ДНК

#+BEGIN_SRC R

library('ade4',lib='./R')
library('genoPlotR',lib='./R')

## Загрузка данных

data(three_genes)

## Вычисление средних позиций

mid_pos <- middle(dna_segs[[1]])

                                        # Create first annotation
annot1 <- annotation(x1=mid_pos, text=dna_segs[[1]]$name)
plot_gene_map(dna_segs=dna_segs, comparisons=comparisons, annotations=annot1)

## Настройки исследования

annot2 <- annotation(x1=c(mid_pos[1], dna_segs[[1]]$end[2]),
                     x2=c(NA, dna_segs[[1]]$end[3]),
                     text=c(dna_segs[[1]]$name[1], "region1"),
                     rot=c(30, 0), col=c("grey", "black"))
plot_gene_map(dna_segs=dna_segs, comparisons=comparisons,
              annotations=annot2, annotation_height=1.3)

## Аннотации на всех сегментах

annots <- lapply(dna_segs, function(x){
    mid <- middle(x)
    annot <- annotation(x1=mid, text=x$name, rot=30)
})

png(file.path('./images', "example_segments.png"), h=300, w=500)

plot_gene_map(dna_segs=dna_segs, comparisons=comparisons,
              annotations=annots, annotation_height=1.8, annotation_cex=1)

#+END_SRC

[[file:./images/example_segments.png]]

**** Ещё пример с Бартонеллой

#+BEGIN_SRC R

library('ade4',lib='./R')
library('genoPlotR',lib='./R')

##
## Используем большой датасет из 4-х геномного сравнения
##

data(barto)

## Добавляем дерево

tree <- newick2phylog("(BB:2.5,(BG:1.8,(BH:1,BQ:0.8):1.9):3);")

## Покажем несколько субсегментов

xlims2 <- list(c(1445000, 1415000, 1380000, 1412000),
               c( 10000, 45000, 50000, 83000, 90000, 120000),
               c( 15000, 36000, 90000, 120000, 74000, 98000),
               c( 5000, 82000))

## Добавление аннотаций ко всем генам + разрешить сегментам показываться снаружи самого длинного

annots <- lapply(barto$dna_segs, function(x){
    mid <- middle(x)
    annot <- annotation(x1=mid, text=x$name, rot=30)
                                        # removing gene names starting with "B" and keeping 1 in 4
    idx <- grep("^[^B]", annot$text, perl=TRUE)
    annot[idx[idx %% 4 == 0],]
})
png(file.path('./images', "barto_seg2.png"), h=300, w=500)
plot_gene_map(barto$dna_segs, barto$comparisons, tree=tree,
              annotations=annots,
              xlims=xlims2,
              limit_to_longest_dna_seg=FALSE,
              dna_seg_scale=TRUE)
#+END_SRC

#+RESULTS:

[[file:./images/barto_seg2.png]]
