(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

 (unless (package-installed-p 'use-package)
   (package-refresh-contents)
   (package-install 'use-package))

 (eval-when-compile
   (require 'use-package))

 (load-theme 'wombat)

;; Краткое y/n
(defalias 'yes-or-no-p 'y-or-n-p)

  (use-package powerline :ensure t 
  :init (powerline-center-theme))

(add-to-list 'display-buffer-alist (cons "\\*Async Shell Command\\*.*" (cons #'display-buffer-no-window nil)))

    (use-package screenshot
    :ensure t
    :bind ("C-c s" . screenshot-take))

     (defvar backup-dir (expand-file-name "~./backup/"))
     (defvar tmp-dir (expand-file-name "~./tmp/"))

     (setq-default make-backup-files t
                   delete-by-moving-to-trash t
                   backup-by-copying nil
                   kept-new-versions 20
                   kept-old-versions 20
                   delete-old-versions t
                   temporary-file-directory tmp-dir
                   small-temporary-file-directory tmp-dir
                   backup-directory-alist `(("" . "~/.emacs.d/backup"))
                   auto-save-file-name-transforms `((".*" ,backup-dir t))
                   auto-save-list-file-prefix backup-dir
                   create-lockfiles nil
                   vc-make-backup-files t
                   version-control t)

     (use-package dired         
       :bind (("C-x d" . dired-jump)
              ("C-x C-d" . dired-jump))
       :init

       (setq dired-listing-switches "-aBhlv  --group-directories-first"
             dired-dwim-target t)

       ;; обновление,только когда не изменились подкаталоги (иначе - долго)
       (setq dired-auto-revert-buffer  (lambda (_dir) (null (cdr dired-subdir-alist))))

       (add-hook 'dired-mode-hook (lambda ()
                                    ;;(diredp-breadcrumbs-in-header-line-mode t)
                                    (define-keys dired-mode-map (list                                                                 
                                                                 "i" nil
                                                                 "j" 'dired-next-line
                                                                 "k" 'dired-previous-line
                                                                 "h" 'dired-up-directory
                                                                 "l" 'dired-find-file
                                                                 (kbd "<down>") 'dired-next-line
                                                                 (kbd "<up>") 'dired-previous-line
                                                                 (kbd "<left>") 'dired-up-directory
                                                                 (kbd "<right>") 'diredp-find-file-reuse-dir-buffer
                                                                 (kbd "M-l") nil
                                                                 (kbd "C-j") 'next-line
                                                                 (kbd "M-+") 'my-dired-create-file
                                                                 (kbd "u") 'dired-get-size
                                                                 (kbd "C-c C-c") 'wdired-change-to-wdired-mode
                                                                 (kbd "M-s") 'helm-do-grep-ag
                                                                 (kbd "C-S-s") 'helm-do-grep-ag))

                                    (hl-line-mode t)))


       (add-hook 'wdired-mode-hook (lambda ()
                                     (define-keys wdired-mode-map (list
                                                                   (kbd "C-j") 'next-line
                                                                   (kbd "C-g C-g") 'wdired-exit
                                                                   )))))

     (use-package dired+
       :ensure t
       :config 

       (define-keys dired-mode-map (list
                                    (kbd "<down>") 'dired-next-line
                                    (kbd "<up>") 'dired-previous-line
                                    (kbd "<left>") 'dired-up-directory
                                    (kbd "<right>") 'diredp-find-file-reuse-dir-buffer
                                    (kbd "RET") 'diredp-find-file-reuse-dir-buffer
                                    (kbd "M-l") nil
                                    )))

     (use-package dired-details 
       :ensure t)

     (use-package dired-details+ 
       :ensure t
       :init (setq dired-details-initially-hide nil))

     (use-package dired-single 
       :ensure t
       :defer t)

     (use-package  ztree
       :ensure t)

     (use-package neotree
       :ensure t
       :defer t
       :bind (("C-c d" . neotree-toggle) ("C-c C-d" . neotree-toggle))
       :init (setq neo-create-file-auto-open t
                   neo-mode-line-type 'none
                   neo-persist-show t
                   neo-window-width 30
                   neo-show-updir-line nil
                   neo-auto-indent-point t
                   neo-banner-message nil
                   neo-modern-sidebar t
                   neo-theme 'icons)

       :config

       (define-keys neotree-mode-map (list
                                      [escape]  'neotree-hide
                                      "l"   'neotree-enter
                                      "j"   'neotree-next-line
                                      "k"   'neotree-previous-line
                                      "h"   'neotree-select-up-node 
                                      (kbd "C-c C-d") 'neotree-toggle
                                      (kbd "C-c C-d") 'neotree-toggle
                                      (kbd "cd") 'neotree-toggle
                                      (kbd "RET")   'neotree-enter
                                      (kbd "<M-return>")   'neotree-change-root
                                      "C"   'neotree-copy-node
                                      "D"   'neotree-delete-node
                                      "R"   'neotree-delete-node
                                      "g"   'neotree-refresh
                                      "q"   'neotree-hide
                                      "r"   'neotree-rename-node
                                      "o"   'neotree-dir
                                      "I"  'neotree-hidden-file-toggle
                                      ))

       (add-hook 'neotree-mode-hook (lambda ()
                                      (hl-line-mode t))))

     (use-package image-mode
       :init
       (remove-hook 'image-mode-hook (lambda ()
                                    (define-keys image-mode-map (list
                                                                 (kbd "C-n") 'image-next-file
                                                                 (kbd "C-p") 'image-previous-file
                                                                 (kbd "n") 'image-next-file
                                                                 (kbd "p") 'image-previous-file
                                                                 (kbd "C-=") 'imagex-sticky-zoom-in
                                                                 (kbd "C-+") 'imagex-sticky-zoom-in
                                                                 (kbd "C--") 'imagex-sticky-zoom-out
                                                                 "+" 'imagex-sticky-zoom-in
                                                                 "-" 'imagex-sticky-zoom-out))

                                    (imagex-global-sticky-mode nil)
                                    (imagex-auto-adjust-mode nil)
                                    )))

(auto-revert-mode t)  ;; Автообновление

(global-set-key (kbd "C-r") (lambda () (interactive) (revert-buffer t t)))

  (use-package shift-text
    :ensure t
    :defer t
     :bind  (("C-M-n" . shift-text-down)
             ("C-M-p" . shift-text-up)
             ("C-M-f" . shift-text-right)
             ("C-M-b" . shift-text-left)))

  (setq-default  indent-tabs-mode nil
                 tab-width 2
                 indent-line-function 'indent-relative
                 default-tabs-width 2)

(setq-default truncate-lines t
              truncate-partial-width-windows 20
              longlines-show-hard-newlines t
              line-move-visual t)

(setq-default scroll-conservatively 101
              scroll-step 0
              scroll-margin 5
              hscroll-step 0
              hscroll-margin 1)

(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C-M-=") 'text-scale-set)
(global-set-key (kbd "C--") 'text-scale-decrease)

(use-package anything :ensure t)

(use-package helm
  :ensure t
  :defer t
  :bind (("M-x" . helm-M-x)
         ("C-x b" . helm-mini)         
         ("C-x C-b" . helm-buffers-list)
         ("C-x C-f" . helm-find-files)
         ("C-x y" . helm-show-kill-ring)
         ("C-c o" . helm-occur)
         ("C-x m" . helm-all-mark-rings)
         ("C-x f" . helm-recentf)
         ("<f1> a" . helm-apropos))
  :init
  (require 'helm-config)
  (helm-mode 1)

  (defun my/helm-fonts ()
    (face-remap-add-relative 'default :height 1.3))

  (add-hook 'helm-major-mode-hook #'my/helm-fonts)

  (setq helm-idle-delay 0.0 ; update fast sources immediately (doesn't).
        helm-input-idle-delay 0.01  ; this actually updates things
                                        ; reeeelatively quickly.
        helm-yas-display-key-on-candidate t
        helm-quick-update t
        helm-M-x-requires-pattern nil
        helm-inherit-input-method nil
        helm-display-header-line nil
        helm-mode-line-string nil
        helm-adaptive-history-length 100
        helm-ff-skip-boring-files t))

(defun helm-display-mode-line (source &optional force) (setq mode-line-format nil))

(use-package helm-swoop
  :ensure t
  :defer t
  :init
  (setq-default helm-swoop-pre-input-function (lambda () ""))
  (global-set-key (kbd "M-s s") 'helm-swoop)
  (global-set-key (kbd "M-s M-s") 'helm-multi-swoop-all)
  )

(use-package helm-smex
  :ensure t
  :defer t
  :init
  (setq-default helm-smex-show-bindings t)
  (global-set-key [remap execute-extended-command] #'helm-smex)
  (global-set-key (kbd "M-X") #'helm-smex-major-mode-commands))

(use-package helm-descbinds
  :ensure t
  :defer t
  :bind (("<f1> w" . helm-descbinds)
         ("<f1> b" . helm-descbinds)))

(use-package helm-ls-hg
  :bind (("C-c hgf" . helm-hg-find-files-in-project))
  )

(use-package helm-ag)

      (setq-default org-hide-emphasis-markers nil)
      (setq-default org-ellipsis "...")
      ;;(add-hook 'org-mode-hook 'turn-on-visual-line-mode)
      (setq-default org-startup-indented nil
                    org-hide-leading-stars nil)

      (use-package org-bullets
        :defer t
        :ensure t
        :init
        (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
        (setq org-bullets-bullet-list '("●" "▲" "■" "⬟" "⬢" "✳")))

      (setq-default org-startup-with-inline-images t)
      (setq-default org-image-actual-width nil)

      (defun my-fix-inline-images ()
        (when org-inline-image-overlays
          (org-redisplay-inline-images)))

      (add-hook 'org-babel-after-execute-hook 'my-fix-inline-images)

      (setq org-hide-block-startup t)

      (setq org-src-fontify-natively t)

       (setq org-support-shift-select nil)

       (setq org-src-tab-acts-natively t
             org-src-preserve-indentation t
             org-edit-src-content-indentation 2)

     (org-babel-do-load-languages
      'org-babel-load-languages
      '((dot . t) 
        (ditaa . t)
        (emacs-lisp . t)
        (plantuml . t)
        (css . t)
        (js . t)
        (R . t)
        (http . t)
        (shell . t)))

     (use-package ox-reveal
       :defer t
       :config
       (setq org-reveal-root "file:///home/az/code/reveal.js/"))

      (defun my/org-inline-css-hook (exporter)
        "Insert custom inline css to automatically set the
      background of code to whatever theme I'm using's background"
        (when (eq exporter 'html)
          (let* ((my-pre-bg (face-background 'default))
                 (my-pre-fg (face-foreground 'default)))
            (setq
             org-html-head-extra
             (concat
              org-html-head-extra
              (format "<style type=\"text/css\">\n pre.src {background-color: %s; color: %s;}</style>\n"
                      my-pre-bg my-pre-fg))))))

      (add-hook 'org-export-before-processing-hook 'my/org-inline-css-hook)

      (setq-default browse-url-browser-function 'browse-url-chromium
                    browse-url-generic-program "chromium")

       (use-package engine-mode 

         :ensure t
         :config

         (setq engine/browser-function 'w3m-goto-url-new-session)

         (engine/set-keymap-prefix (kbd "C-c e"))

         (defengine amazon
           "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=%s")

         (defengine aliexpress
           "https://ru.aliexpress.com/af/%s.html"
           :browser 'browse-url-generic
           :keybinding "ali")

         (defengine duckduckgo
           "https://duckduckgo.com/?q=%s"
           :keybinding "dg")

         (defengine github
           "https://github.com/search?ref=simplesearch&q=%s"
           :keybinding "gh")

         (defengine google
           "http://www.google.com/search?ie=utf-8&oe=utf-8&q=%s"
           :keybinding "go")

         (defengine google-images
           "http://www.google.com/images?hl=en&source=hp&biw=1440&bih=795&gbv=2&aq=f&aqi=&aql=&oq=&q=%s"
           :keybinding "gi")

         (defengine google-maps
           "http://maps.google.com/maps?q=%s"
           :keybinding "gm"    
           :docstring "Mappin' it up.")

         (defengine project-gutenberg
           "http://www.gutenberg.org/ebooks/search.html/?format=html&default_prefix=all&sort_order=&query=%s")

         (defengine rfcs
           "http://pretty-rfc.herokuapp.com/search?q=%s")

         (defengine so
           "https://stackoverflow.com/search?q=%s"
           :keybinding "so"    )

         (defengine ru-so
           "https://ru.stackoverflow.com/search?q=%s"
           :keybinding "rso"    )

         (defengine twitter
           "https://twitter.com/search?q=%s"
           :keybinding "tw"    )

         (defengine wikipedia
           "http://www.wikipedia.org/search-redirect.php?language=ru&go=Go&search=%s"
           :keybinding "wi"
           :docstring "Searchin' the wikis.")

         (defengine wiktionary
           "https://www.wikipedia.org/search-redirect.php?family=wiktionary&language=en&go=Go&search=%s")

         (defengine wolfram-alpha
           "http://www.wolframalpha.com/input/?i=%s")

         (defengine youtube
           "http://www.youtube.com/results?aq=f&oq=&search_query=%s"
           :keybinding "yo")

         (engine-mode))

       (use-package google-this
         :ensure t
         :defer t
         :init
         (global-set-key (kbd "C-c C-g") 'google-this))

       (use-package elfeed
         :ensure t
         :defer t
         :config
         (define-keys elfeed-search-mode-map (list
                                              (kbd "C-j") 'next-line
                                              (kbd "C-k") 'previous-line
                                              (kbd "C-l") 'elfeed-search-show-entry
                                              (kbd "j") 'next-line
                                              (kbd "k") 'previous-line
                                              (kbd "l") 'elfeed-search-show-entry))
         (define-keys elfeed-show-mode-map (list
                                            (kbd "h") 'elfeed-kill-buffer ))  
         (setq elfeed-feeds

               '(("https://lenta.ru/rss" news russia)
                 ("https://actualidad.rt.com/feed" news russia)
                 ("http://static.feed.rbc.ru/rbc/logical/footer/news.rss" news)
                 ("https://meduza.io/rss/all" news russia)
                 ("http://newsbabr.com/rss/news.xml" news irkutsk)
                 ("https://www.irk.ru/news.rss" news irkutsk)
                 ("https://postnauka.ru/feed" science)
                 ("https://gorky.media/feed/" science)
                 ("http://news.rambler.ru/rss/world/" news world)
                 ("http://news.yandex.ru/index.rss" news world)
                 ("http://www.aif.ru/rss/all.php" news world russia)
                 ("https://www.vz.ru/rss.xml" news russia)
                 ("https://www.consultant.ru/rss/hotdocs.xml" news law russia)
                 ("https://www.consultant.ru/rss/fd.xml" law russia)
                 ("https://www.consultant.ru/rss/nw.xml" law russia))
               elfeed-search-filter "@2-days-ago +unread"))

     (use-package wttrin
       :ensure t
       :defer t
       :commands (wttrin)
       :bind (("<f1> W" . wttrin))
       :init
       (setq wttrin-default-accept-language '("Accept-Language" . "ru-RU,ru")
             wttrin-default-cities '("Moscow"
                                     "Novosibirsk"
                                     "Krasnoyarsk"
                                     "Irkutsk"
                                     "Angarsk"
                                     "Voronezh"
                                     "Rossosh")))

         (use-package howdoi
           :ensure t
           :defer t
           :bind* (("C-c h d q" . howdoi-query)
                   ("C-c h d p" . howdoi-query-line-at-point)
                   ("C-c h d s" . howdoi-query-insert-code-snippet-at-point)))

         (use-package sx
           :defer t
           :ensure t)

       (use-package google-maps
         :ensure t
         :defer t
         :init
         (require 'google-maps-static)
         (define-keys google-maps-static-mode-map (list
                                                   "h" 'google-maps-static-move-west
                                                   "j" 'google-maps-static-move-south
                                                   "k" 'google-maps-static-move-north
                                                   "l" 'google-maps-static-move-east
                                                   )))
